"""
script for abstract class
"""
from abc import ABC, abstractmethod


class Country(ABC):
    """
    abstract class
    """
    @abstractmethod
    def statistics_on_job(self, count):
        """
        information about hor many job-offers
        """

    @abstractmethod
    def number_of_population(self, count):
        """
        information about population
        """

    @abstractmethod
    def statistics_about_education(self, count):
        """
        information about hor many people have higher education
        """


class Perm(Country):
    """
    typical class
    """
    def __init__(self):
        self.job = 0
        self.population = 0
        self.education = 0

    def statistics_about_education(self, count):
        self.education += count

    def number_of_population(self, count):
        self.population += count

    def statistics_on_job(self, count):
        self.job += count


def main():
    """
    main function
    """
    city = Perm()
    city.number_of_population(1000)
    city.statistics_about_education(100)
    city.statistics_on_job(10)
    print('population growth is 1000, now population:', city.population, '\n',
          'education growth is 100, now people how have higher education:',
          city.education, '\n ', 'job growth is 10, now count job-offer:', city.job)


if __name__ == "__main__":
    main()
