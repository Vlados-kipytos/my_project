#!/usr/bin/env python
"""
script for create random binary file and for convert this binary file in 'utf-8'
and subsequently analyse this file on count lines,words,letters
"""
import random
import string
import re


def generate_random_bin_file(size):
    """
    function for generate random binary file
    """
    random_string = (
        ''.join(random.choice(
            string.ascii_uppercase + string.ascii_lowercase +
            string.digits + string.punctuation + string.whitespace)
                for _ in range(size)))
    string_to_byte = random_string.encode()
    with open('testing.bin', 'wb') as file:
        file.write(string_to_byte)


def read_random_bin_file_and_analyse():
    """
    function for read and analyse file
    """
    lines = 0
    words = 0
    letters = 0
    with open('testing.bin', 'rb') as file:
        line = file.readline().decode('utf-8')
        while line:
            letters += len(line)
            words += len(list(filter(None, re.split('[!"#$%&()*+,-./:;<=>?@^_`{|}~\n]', line))))
            line = file.readline().decode('utf-8')
            lines += 1
    print("Lines:", lines)
    print("Words:", words)
    print("Letters:", letters)


if __name__ == "__main__":
    COUNT_FILES = 100
    generate_random_bin_file(COUNT_FILES)
    read_random_bin_file_and_analyse()
