#!/usr/bin/env python
"""
script for analyse directory on count file in this directory
"""
import os
import argparse


def parser_logik():
    """
    function for parser_logik
    """
    parser = argparse.ArgumentParser(description='Script to get information about directory')
    parser.add_argument('-p', '--path', required=True, type=str, help='The path to the file')
    parser.add_argument('-H', '--hide', required=True, type=bool,
                        help='True if you want to hide files, False if you dont want')
    args = parser.parse_args()
    return args


def getting_structure(args):
    """
    function for getting_structure
    """
    list_dirs = os.walk(args.path, topdown=True)
    return list_dirs


def output(list_dirs):
    """
    function for output
    """
    for root, dirs, files in list_dirs:
        if not ARGS.hide:
            files = [f for f in files if not f[0] == '.']
            dirs[:] = [d for d in dirs if not d[0] == '.']
        for d in dirs:
            if os.path.isdir(d):
                print(os.path.join(root, d), os.path.getsize(d), 'байт')
        for f in files:
            if os.path.isfile(f):
                print(os.path.join(root, f), os.path.getsize(f), 'байт')


if __name__ == "__main__":
    ARGS = parser_logik()
    LIST_DIRS = getting_structure(ARGS)
    output(LIST_DIRS)
