"""
Script for week4
"""


class NegValException(Exception):
    """
    My exception
    """

    def __init__(self, text):
        self.txt = text


class IPhone:
    """
    parent class
    """

    def __str__(self):
        return " class Object"

    def __init__(self, age):
        """
        constructor
        """
        self.style = "Phone"  # public
        self.__serial_number = 'SJQJK23'
        self.country = 'USA'
        self.age = age
        if self.age < 0:
            raise NegValException('age < 0')
        self._shell = 'Classic'  # protected
        self.__weight = 113  # private

    @staticmethod
    def get_class_details():
        """
        static method
        """
        print("This is static method")

    def __eq__(self, other):
        return self.age == other.age

    def __add__(self, other):
        return IPhone(self.age + other.age)

    def __call__(self):
        return 'Information about type of phone', self.style

    def your_mind(self, mind, max_mind):
        """
        method for rate
        """
        print('Your_mind', mind / max_mind)


class IphoneSE(IPhone):
    """
    child class
    """

    def __init__(self, age):
        super().__init__(age)
        self.style = 'IphoneSE'

    def __call__(self):
        return 'Information about type of phone', self.style


class Iphone10(IPhone):
    """
    child class
    """

    def __init__(self, age):
        super().__init__(age)
        self.style = "Iphone10"

    def __call__(self):
        return 'Information about type of phone', self.style


if __name__ == "__main__":
    VAL = int(input("input positive number of your IPhone: "))
    try:
        if VAL < 0 or VAL != 7:
            raise NegValException("Правильный формат номера: (8793829):, ваш номер:" + str(VAL))
        print('+7(919)', VAL)
    except NegValException as error:
        print(error)

    phone1 = IphoneSE(2)
    try:
        phone1.your_mind(2, 0)
    except ZeroDivisionError:
        print('Вы указали 0 в максимальной оценке')
    try:
        phone1.your_mind('dsds', 5)
    except TypeError as error:
        print(error)

    phone2 = Iphone10(3)
    phone2.your_mind(3, 4)
