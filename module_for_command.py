"""
script for pattern implementation
"""
import os
from stat import ST_MODE
from time import sleep


class ReadFileCommand:
    """
    class for reading file
    """

    def __init__(self, name, time=0):
        self._time = time
        self._name = name

    def execute(self):
        """
        function for execute
        """
        sleep(self._time)
        with open(self._name, 'r') as file:
            data = file.read()
            print(data)

    def undo(self):
        """
        function for undo
        """
        pass


class WriteFileCommand:
    """
    class for writing file
    """

    def __init__(self, name, new_data, time=0):
        self._time = time
        self._name = name
        self._new_data = new_data
        with open(self._name, 'r') as file:
            self._old_data = file.read()

    def execute(self):
        """
        function for execute
        """
        sleep(self._time)
        with open(self._name, 'a') as file:
            file.write(self._new_data)

    def undo(self):
        """
        function for undo
        """
        sleep(self._time)
        with open(self._name, 'w') as file:
            file.write(self._old_data)


class DeleteFileCommand:
    """
    class for delete file
    """

    def __init__(self, name, time=0):
        self._time = time
        self._name = name
        with open(self._name, 'r') as file:
            self._old_data = file.read()

    def execute(self):
        """
        function for execute
        """
        sleep(self._time)
        os.remove(self._name)

    def undo(self):
        """
        function for undo
        """
        sleep(self._time)
        with open(self._name, 'w') as file:
            file.write(self._old_data)


class Create_Permission:
    """
    class for create permission file
    """

    def __init__(self, name, permission, time=0):
        Octal_Number = list((oct(os.stat(name)[ST_MODE])[-3:]))
        self._time = time
        self._name = name
        self._old_permission = (int(Octal_Number[0]) * (pow(8, 2))) + \
                               (int(Octal_Number[1]) * (pow(8, 1))) + \
                               (int(Octal_Number[2]) * (pow(8, 0)))
        self._new_permission = permission

    def execute(self):
        """
        function for execute
        """
        sleep(self._time)
        os.chmod(self._name, self._new_permission)

    def undo(self):
        """
        function for undo
        """
        sleep(self._time)
        os.chmod(self._name, self._old_permission)


class History:
    """
    class for log of command
    """

    def __init__(self):
        self._commands = list()

    def execute(self, command):
        """
        function for execute
        """
        self._commands.append(command)
        command.execute()

    def more_execute(self, myList=[]):
        """
        function for more execute
        """
        for values in myList:
            values.execute()

    def undo(self):
        """
        function for undo
        """
        self._commands.pop().undo()

    def more_undo(self, myList=[]):
        """
        function for more_undo
        """
        for values in myList:
            values.undo()


def main():
    """
    main function
    """
    history = History()
    FILEDIR = os.path.dirname(os.path.abspath(__file__))
    NAME_OF_YOUR_FILE = 'example.txt'
    if os.uname()[0] == 'Linux':
        NAME_OF_YOUR_FILE = '/' + NAME_OF_YOUR_FILE
    elif os.uname()[0] == 'Windows':
        NAME_OF_YOUR_FILE = '\example.txt'
    LIST_OF_OBJECT = [WriteFileCommand(FILEDIR + NAME_OF_YOUR_FILE, '\nHell\nHello'),
                      ReadFileCommand(FILEDIR + NAME_OF_YOUR_FILE)]
    history.more_execute(LIST_OF_OBJECT)
    history.more_undo(LIST_OF_OBJECT)

    history.execute(WriteFileCommand(FILEDIR + NAME_OF_YOUR_FILE, '\nHell\nHello'))
    history.undo()

    history.execute(ReadFileCommand(FILEDIR + NAME_OF_YOUR_FILE))

    history.execute(DeleteFileCommand(FILEDIR + NAME_OF_YOUR_FILE))
    history.undo()

    # print((oct(os.stat(FILEDIR + NAME_OF_YOUR_FILE)[ST_MODE])[-3:]))

    history.execute(Create_Permission(FILEDIR + NAME_OF_YOUR_FILE, 0o777))
    history.undo()

    # print((oct(os.stat(FILEDIR + NAME_OF_YOUR_FILE)[ST_MODE])[-3:]))


if __name__ == "__main__":
    main()
