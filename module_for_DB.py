"""
script for create tables and for working with tables in database Postgres
"""
from random import randint
import random
import string
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, VARCHAR, ForeignKey, func
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy.orm.exc import NoResultFound

Base = declarative_base()


class BaseModel(Base):
    """
    Base class for inherit required in child class
    """
    __abstract__ = True

    id = Column(Integer, nullable=False, unique=True, primary_key=True, autoincrement=True)

    def __repr__(self):
        return "<{0.__class__.__name__}(id={0.id!r})>".format(self)


class Employee(BaseModel):
    """
    Class for table employees
    """
    __tablename__ = 'employees'

    first_name = Column(VARCHAR(255), nullable=False)
    last_name = Column(VARCHAR(255), nullable=False)
    phone = Column(Integer)
    description = Column(VARCHAR(255), nullable=True)


class Department(BaseModel):
    """
    Class for table departments
    """
    __tablename__ = 'departments'

    name = Column(VARCHAR(255), nullable=False)
    description = Column(VARCHAR(255), nullable=False)


class EmployeeDepartments(BaseModel):
    """
    Class for table employee_departments
    """
    __tablename__ = 'employee_departments'

    employee_id = Column(Integer, ForeignKey('employees.id', ondelete='CASCADE'),
                         nullable=False, index=True)
    department_id = Column(Integer, ForeignKey('departments.id', ondelete='CASCADE'),
                           nullable=False, index=True)


def AdditionRandom_Employee(count, session):
    """
    function to create random values for table employees
    """
    for num in range(count):
        employee = Employee(
            first_name=''.join(random.choice(string.ascii_letters) for _ in range(4)),
            last_name=''.join(random.choice(string.ascii_letters) for _ in range(5)),
            phone=randint(0, 500),
            description=''.join(random.choice(string.ascii_letters) for _ in range(6)),
        )
        session.add(employee)
        session.commit()


def AdditionRandom_Department(count, session):
    """
    function to create random values for table departments
    """
    for num in range(count):
        department = Department(
            name=''.join(random.choice(string.ascii_letters) for _ in range(4)),
            description=''.join(random.choice(string.ascii_letters) for _ in range(5)),
        )
        session.add(department)
        session.commit()


def AdditionRandom_DepartmentEmployee(count, session):
    """
    function to create random values for table employee_departments
    """
    for num in range(count):
        employee_department = EmployeeDepartments(
            employee_id=randint(1, 8),
            department_id=randint(1, 8),
        )
        session.add(employee_department)
        session.commit()


def recreate_database():
    """
    function for create all values or drop all values in tables
    """
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)


def main():
    """
    main function
    """
    AdditionRandom_Employee(10, session)
    AdditionRandom_Department(10, session)
    AdditionRandom_DepartmentEmployee(4, session)

    New_Employee = Employee(
        first_name='Vladislav',
        last_name='Malyshev',
        phone='3213',
        description='coolman',
    )

    New_Department = Department(
        name='IT',
        description='cooldepartment',
    )

    New_DepartmentEmployee = EmployeeDepartments(
        employee_id='8',
        department_id='10',
    )

    # add new values
    session.add(New_Employee, New_Department)
    session.add(New_DepartmentEmployee)

    # update
    session.query(Employee).filter(Employee.first_name == 'Vladislav'). \
        update({Employee.first_name: 'Vadim'}, synchronize_session=False)

    # delete
    session.query(Employee).filter(Employee.id == 2). \
        delete(synchronize_session=False)

    session.commit()

    # full information about table employees
    for id, first_name, last_name, phone, description \
            in session.query(Employee.id, Employee.first_name,
                             Employee.last_name, Employee.phone,
                             Employee.description):
        print(id, first_name, last_name, phone, description)

    # join
    for first_name, last_name, department_id in \
            session.query(Employee.first_name, Employee.last_name,
                          EmployeeDepartments.department_id).join(EmployeeDepartments):
        print(first_name, last_name, department_id)

    # having count
    for first_name in session.query(Employee.first_name).group_by(Employee.id).having(
            func.count(Employee.phone) > 1):
        print(first_name)

    # filter_by
    for first_name in session.query(Employee.first_name).filter_by(last_name='Petrov'):
        print(first_name)

    # union
    query1 = session.query(Employee.first_name).filter_by(last_name='Petrov')
    query2 = session.query(Employee.first_name).filter_by(phone='212')
    query3 = query1.union(query2)

    # one
    try:
        print(session.query(Employee).filter(Employee.first_name == "Alexander").one())
    except NoResultFound as error:
        print(error)

    # all
    print(session.query(Employee.first_name).all())


if __name__ == '__main__':
    NAME_SERVER = 'postgres'
    PASSWORD = 'vladik'
    NAME_DB = 'postgres'
    DATABASE_URI = F'postgres+psycopg2://{NAME_SERVER}:{PASSWORD}@localhost:5432/{NAME_DB}'
    engine = create_engine(DATABASE_URI)
    recreate_database()
    Session = sessionmaker(bind=engine)
    Session.configure(bind=engine)
    session = Session()
    main()
    engine.dispose()
