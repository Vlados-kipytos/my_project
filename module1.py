"""This script is written to create three functions."""


def list_to_dict(mylist):
    """Function to convert a list to a dictionary."""
    mydict = {i: mylist.count(i) for i in mylist}
    return mydict


def strings_interspec(str1, str2):
    """Function should return a set of words at the intersection of lines."""
    return set(str1.lower().split()).intersection(set(str2.lower().split()))


def longest_ascending_seq(mylist):
    """A function that from the input list of numbers returns the
    largest (in length) ascending sequence (also in theform of a list) """
    mylist = [part for num, part in enumerate(mylist) if (mylist[-1] != part
                                                          and mylist[num + 1] - mylist[num] == 1
                                                          and mylist[num] - mylist[num - 1] <= 1)
              or (part == mylist[-1] and mylist[num] - mylist[num - 1] == 1)]
    for num, part in enumerate(mylist):
        if mylist[-1] != part and mylist[num + 1] - mylist[num] != 1:
            mylist.remove(mylist[num])
    return mylist
