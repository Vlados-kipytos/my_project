"""
script for interface
"""
import zope.interface


class Country(zope.interface.Interface):
    """
    class-interface
    """
    y = zope.interface.Attribute("""Interface""")

    def statistics_on_job(self, count):
        """
        information about hor many job-offers
        """

    def number_of_population(self, count):
        """
        information about population
        """

    def statistics_about_education(self, count):
        """
        information about hor many people have higher education
        """


@zope.interface.implementer(Country)
class Perm:
    """
    class for class-interface
    """

    def __init__(self):
        self.job = 0
        self.population = 0
        self.education = 0

    def statistics_about_education(self, count):
        """
        method for add count of people who have higher education
        """
        self.education += count

    def number_of_population(self, count):
        """
        method for add count of population
        """
        self.population += count

    def statistics_on_job(self, count):
        """
        method for add count job-offer
        """
        self.job += count


def main():
    """
    main function
    """
    city = Perm()
    city.number_of_population(1000)
    city.statistics_about_education(100)
    city.statistics_on_job(10)
    print('population growth is 1000, now population:', city.population, '\n',
          'education growth is 100, now people how have higher education:',
          city.education, '\n ', 'job growth is 10, now count job-offer:', city.job)
    # print(Country.implementedBy(Perm))
    # print(Country.providedBy(city))


if __name__ == "__main__":
    main()
