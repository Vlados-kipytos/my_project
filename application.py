"""
script for week3
"""
import re
import json
from urllib.parse import parse_qs
import requests


def repos(environ, start_response):
    """
    Function for connect to github and get information about projects
    """
    start_response('200 OK', [('Content-Type', 'application/json')])
    with requests.Session() as session:
        token = '8b37dce2d5ae4b5aab7aa344f9ec974b9eb592b7'
        headers = {'Authorization': 'token ' + token}
        login = session.get('https://api.github.com/user', headers=headers)
        repos_info = session.get('https://api.github.com/users/vladislav1997-afk/repos')
        inf_log = login.json()
        inf_rep = repos_info.json()
        lis = []
        dic = {}
        for i in range(inf_log['public_repos']):
            dic.update(html_url=inf_rep[i]['html_url'])
            dic.update(name=inf_rep[i]['name'])
            dic.update(size=inf_rep[i]['size'])
            dic.update(private=inf_rep[i]['private'])
            dic.update(watchers_count=inf_rep[i]['watchers_count'])
            lis.append(dic.copy())
        data = json.dumps(lis)
        return [data.encode()]


def forbidden(environ, start_response):
    """
    403 Forbidden
    """
    start_response('403 Forbidden', [('Content-Type', 'text/html')])
    return [bytes('Sorry, access denied', 'utf-8')]


def index(environ, start_response):
    """This function will be mounted on "/" and display a link
    to the hello world page."""
    start_response('200 OK', [('Content-Type', 'text/html')])
    return [bytes('Hello world', 'utf-8')]


def hello(environ, start_response):
    """Like the example above, but it uses the name specified in the
URL."""
    start_response('200 OK', [('Content-Type', 'text/html')])
    return [bytes('Hello world', 'utf-8')]


def not_found(environ, start_response):
    """Called if no URL matches."""
    start_response('404 NOT FOUND', [('Content-Type', 'text/plain')])
    return [bytes('Not Found', 'utf-8')]


def not_working(environ, start_response):
    """
     521 Web Server Is Down
    """
    start_response('521 Web Server Is Down', [('Content-Type', 'text/plain')])
    return [bytes('Sorry, server not working', 'utf-8')]


# map urls to functions
URLS = [
    (r'^$', index),
    (r'hello/?$', hello),
    (r'hello/(.+)$', hello),
    (r'security', forbidden),
    (r'test', not_working),
    (r'repos', repos)
]


def application(environ, start_response):
    """
    The main WSGI application.
    If nothing matches call the `not_found` function.
    """
    d = parse_qs(environ['QUERY_STRING'])
    path = environ.get('PATH_INFO', '').lstrip('/')
    for regex, callback in URLS:
        match = re.search(regex, path)
        if d.get('security') == ['test']:
            return forbidden(environ, start_response)
        elif match is not None:
            return callback(environ, start_response)
    return not_found(environ, start_response)
