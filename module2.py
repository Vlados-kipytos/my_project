"""The script is written to invocation module1 and check all its functions"""
import module1
from module1 import list_to_dict as ltd
from module1 import strings_interspec as si
from module1 import longest_ascending_seq as la


LST = ['m', 1, 'b', 2, 'k', 3, 3, 3, 'k', 'k', 'b']
LST2 = [1, 2, -3, 0, 30, 4, 5, 6]
STR1 = 'Simple word'
STR2 = 'Bad Word'
print(module1.list_to_dict(LST))
print(module1.strings_interspec(STR1, STR2))
print(module1.longest_ascending_seq(LST2))
print(ltd(LST))
print(si(STR1, STR2))
print(la(LST2))
