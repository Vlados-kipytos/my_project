"""
script about working module python-crontab
"""
from crontab import CronTab

cron = CronTab(user='vladialv')

job1 = cron.new(command='python Python-crontab.py')
job1.hour.every(3)

job2 = cron.new(command='python Python-crontab.py')
job2.hour.also.on(15)
job2.minute.also.on(15)

job3 = cron.new(command='python Python-crontab.py')
job3.hour.also.on(0)
job3.minute.also.on(0)
job3.dow.on('SUN')

for item in cron:
    print(item)


