"""
script about working iterators and generators in python
"""


class Fibonacci:
    """
    Iterator
    """

    def __init__(self, limit):
        self.limit = limit
        self.finish = 0
        self.last = 1
        self.next = 0
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            self.finish = self.next
            self.next += self.last
            self.last = self.finish
            return self.next
        else:
            raise StopIteration


my_iter = Fibonacci(11)
for i in my_iter:
    print(i)


def fibonacci(count):
    """
    Generator
    """
    value = 0
    last_value = 1
    next_value = 1
    while value < count:
        yield last_value
        last_value, next_value = next_value, last_value + next_value
        value += 1


my_gen = fibonacci(5)
print(next(my_gen))
print(next(my_gen))
print(next(my_gen))
print(next(my_gen))
print(next(my_gen))
