"""
script about working decorators in python
"""
import time


def method_decor(fn):
    """
    method decorator
    """

    def wrapper(self, mind, max_mind):
        print("Run method: " + str(fn.__name__))
        start = time.time()
        try:
            fn(self, mind, max_mind)
        except ZeroDivisionError as error:
            print(error)
        end = time.time()
        print("Function Execution time:", (end - start))

    return wrapper


class NegValException(Exception):
    """
    My exception
    """

    def __init__(self, text):
        self.txt = text


class IPhone:
    """
    parent class
    """

    def __str__(self):
        return " class Object"

    def __init__(self, age):
        """
        constructor
        """
        self.style = "Phone"  # public
        self.__serial_number = 'SJQJK23'
        self.country = 'USA'
        self.age = age
        if self.age < 0:
            raise NegValException('age < 0')
        self._shell = 'Classic'  # protected
        self.__weight = 113  # private

    @staticmethod
    def get_class_details():
        """
        static method
        """
        print("This is static method")

    def __eq__(self, other):
        return self.age == other.age

    def __add__(self, other):
        return IPhone(self.age + other.age)

    def __call__(self):
        return 'Information about type of phone', self.style

    @method_decor
    def your_mind(self, mind, max_mind):
        """
        method for rate
        """
        print('Your_mind', mind / max_mind)


phone = IPhone(10)
phone.your_mind(2, 0)
